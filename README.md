Response codes
200 OK

201 Created

204 No Content

400 Bad Request

401 Unauthorized

404 Page not Found

500 Internal Server error